package baitap1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class array {

    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int bai;
        do {
            System.out.println("Nhập bài muốn chạy:");
            bai = sc.nextInt();

            switch (bai){
                case 2 :
                    bai2();
                    break;
                case 3:
                    bai3();
                    break;
            }
        }while (bai!=0);
    }

    public static void bai2(){
        ArrayList<Integer> array = new ArrayList<>();
        System.out.println("Nhập số lượng số nguyện: ");
        int n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.println("Nhập số nguyên: ");
            int a = sc.nextInt();
            array.add(a);
        }
        System.out.print("Dãy số trong mảng chưa sắp xếp:");

        for (int i = 0; i < array.size(); i++) {
            System.out.print(array.get(i) + " ");
        }

        System.out.print("Dãy số trong mảng sau khi sắp xếp:");

        Collections.sort(array);
        for (int i = 0; i < array.size(); i++) {
            System.out.print(array.get(i) + " ");
        }

    }

//    public static void nhapSoLuong(){
//        System.out.println("Nhập số lương số nguyên");
//        int n = sc.nextInt();
//        Array[] list = new Array[n];
//    }
    public static void bai3(){
        System.out.println("Nhập số lương số nguyên");
        int n = sc.nextInt();
        int[] list = new int[n];

        for (int i = 0; i < list.length; i++) {
            System.out.print("Nhập số nguyên:");
            list[i] = sc.nextInt();

        }
        System.out.print("Các phần tử trong mảng là: ");
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i] + " ");
        }
        System.out.println("");
        int count = 0;
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list.length; j++) {
                if (list[j] == list[i]) {
                    count++;
                }
            }
            System.out.println("Phần tử có giá trị " + list[i] + " xuất hiện " + count + " lần ");
            count = 0 ;
        }

    }
}
